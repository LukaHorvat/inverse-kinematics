{-# LANGUAGE RecordWildCards, ExistentialQuantification, RankNTypes, TemplateHaskell #-}
{-# LANGUAGE ScopedTypeVariables, StandaloneDeriving, DeriveAnyClass #-}
module Main where

import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Control.Lens
import Control.Lens.At
import Data.Map (Map)
import qualified Data.Map as Map

numArms :: Int
numArms = 10

armLength :: Float
armLength = 100 / fromIntegral numArms

data State = State { _angles :: Map Int Float }
                   deriving (Eq, Ord, Read, Show)
makeLenses ''State

data FracLens s = forall a. Fractional a => FracLens { _fracLens :: Lens' s a }
makeLenses ''FracLens

distance :: Point -> Point -> Float
distance (x1, y1) (x2, y2) = sqrt ((x2 - x1) ** 2 + (y2 - y1) ** 2)

just :: Functor f => (a -> f a) -> Maybe a -> f (Maybe a)
just = lens (\(Just a) -> a) (\(Just _) a -> Just a)

unsafeIx :: Int -> Lens' (Map Int a) a
unsafeIx n = at n . just

class Diffable a where
    dimensions :: [FracLens a]

instance Diffable State where
    dimensions = map (\i -> FracLens (angles . unsafeIx i)) [1..numArms]

data AnyFractional = AnyFractional (forall f. Fractional f => f)

toAnyFractional :: Real f => f -> AnyFractional
toAnyFractional r = AnyFractional (realToFrac r)

descend :: Diffable s => s -> (s -> Point) -> Point -> [s]
descend s f wanted = newState : descend newState f wanted
    where diffOn l = distance wanted (f (s & l %~ (+ 0.001)))
                   - distance wanted (f (s & l %~ subtract 0.001))
          ds = map (\(FracLens f) -> toAnyFractional (diffOn f)) dimensions
          newState = foldl (\s' (FracLens l, AnyFractional x) -> s' & l %~ subtract x)
                           s (zip dimensions ds)

allPoints :: State -> [Point]
allPoints s = scanl (\(x, y) i ->
                       let Just angle = s ^. angles . at i in
                       (x + armLength * cos angle, y + armLength * sin angle))
                   (0, 0) [1..numArms]

endPoint :: State -> Point
endPoint = last . allPoints

renderState :: State -> Picture
renderState = line . allPoints

updateState :: Event -> State -> State
updateState (EventMotion wanted) st = descend st endPoint wanted !! 100
updateState _ st = st

main = play (InWindow "Nice Window" (200, 200) (10, 10)) white 60
            (State (Map.fromList $ zip [1..] (map (* 0.1) [1..fromIntegral numArms]))) renderState
            updateState
            (\_ w -> w)
